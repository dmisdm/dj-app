const youtubedl = require("youtube-dl");
const port = process.env.PORT || 3001;
const express = require("express");

const cors = require("cors");
const app = express();

app.use(cors());
app.use("/link-proxy", (req, res, next) => {
  const link = req.query["link"];
  if (!link) {
    return next();
  }

  const video = youtubedl(link);
  video.pipe(res);
});

app.listen(port, () => {
  console.log("Listening on " + port);
});
