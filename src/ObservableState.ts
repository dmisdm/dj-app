type Listener<S> = (state: S) => void;
export class ObservableState<S> {
  private _state: S;
  get state() {
    return this._state;
  }
  constructor(initialState: S) {
    this._state = initialState;
  }

  private changeListeners: Array<Listener<S>> = [];
  addChangeListener = (listener: Listener<S>) => {
    this.changeListeners.push(listener);
  };

  removeChangeListener = (listener: Listener<S>) => {
    this.changeListeners = this.changeListeners.filter((l) => l !== listener);
  };

  setState(state: S) {
    this._state = state;
    this.changeListeners.forEach((listener) => {
      setImmediate(() => {
        listener(state);
      });
    });
  }
}
