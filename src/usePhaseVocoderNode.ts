import React from "react";
import { guess } from "web-audio-beat-detector";
import { AudioPlayer } from "./PhaseVocoder/AudioPlayer";

interface State {
  tempo: number;
  secondsPlayed: number;
  duration: number;
  percentagePlayed: number;
  pitch: number;
  playing: boolean;
  bpmStartOffset: number;
  bpm: number;
  baseBpm: number;
  volume: number;
}
export function usePhaseVocoderNode(
  audioContext: AudioContext,
  destination: AudioNode,
  rawAudioArrayBuffer?: ArrayBuffer
) {
  const [state, setState] = React.useState<State>();
  const audioPlayerRef = React.useRef<AudioPlayer>();
  const gainNodeRef = React.useRef<GainNode>();

  React.useEffect(() => {
    let cancelled = false;
    if (rawAudioArrayBuffer) {
      audioContext.decodeAudioData(rawAudioArrayBuffer).then((audioBuffer) => {
        if (cancelled) return;
        guess(audioBuffer)
          .catch((e) => {
            console.error("Couldn't determine bpm", e);
            return { bpm: 100, offset: 0 };
          })
          .then(({ bpm, offset }) => {
            if (cancelled) return;
            const gainNode = audioContext.createGain();
            gainNodeRef.current = gainNode;
            audioPlayerRef.current = new AudioPlayer({
              audioBuffer,
              audioContext,
              destination: gainNode,
            });

            //audioPlayerRef.current.time = offset;
            gainNode.connect(destination);
            setState({
              bpm,
              baseBpm: bpm,
              bpmStartOffset: offset,
              tempo: 1,
              volume: 1,
              playing: false,
              duration: audioPlayerRef.current.duration,
              secondsPlayed: audioPlayerRef.current.time,
              percentagePlayed:
                audioPlayerRef.current.time / audioPlayerRef.current.duration,
              pitch: 1,
            });
          });
      });
    }

    return () => {
      cancelled = true;
    };
  }, [audioContext, destination, rawAudioArrayBuffer]);

  const play = React.useCallback(() => {
    audioPlayerRef.current?.play();
    setState((state) =>
      state
        ? {
            ...state,
            playing: true,
          }
        : state
    );
  }, []);
  const pause = React.useCallback(() => {
    audioPlayerRef.current?.stop();
    setState((state) =>
      state
        ? {
            ...state,
            playing: false,
          }
        : state
    );
  }, []);

  const seek = React.useCallback((position: number) => {
    if (audioPlayerRef.current) audioPlayerRef.current.time = position;
  }, []);

  const setStretchFactor = React.useCallback((stretchFactor: number) => {
    if (audioPlayerRef.current) {
      audioPlayerRef.current.stretch = 1 / stretchFactor;
      setState((state) =>
        state
          ? {
              ...state,
              tempo: stretchFactor,
              bpm: state.baseBpm * stretchFactor,
            }
          : state
      );
    }
  }, []);

  const setVolume = React.useCallback((volume: number) => {
    if (gainNodeRef.current) {
      gainNodeRef.current.gain.value = volume;
      setState((state) =>
        state
          ? {
              ...state,
              volume,
            }
          : state
      );
    }
  }, []);

  const nudgeForward = React.useCallback(() => {
    if (audioPlayerRef.current) {
      const audioPlayer = audioPlayerRef.current;

      audioPlayer.stretch = audioPlayerRef.current.stretch - 0.1;
      setTimeout(() => {
        audioPlayer.stretch = audioPlayer.stretch + 0.1;
      }, 40);
    }
  }, []);
  const nudgeBackward = React.useCallback(() => {
    if (audioPlayerRef.current) {
      const audioPlayer = audioPlayerRef.current;

      audioPlayer.stretch = audioPlayerRef.current.stretch + 0.1;
      setTimeout(() => {
        audioPlayer.stretch = audioPlayer.stretch - 0.1;
      }, 40);
    }
  }, []);

  const baseBpm = state?.baseBpm;
  const bpmStartOffset = state?.bpmStartOffset;

  const setBPM = React.useCallback(
    (bpm: number) => {
      if (audioPlayerRef.current && baseBpm) {
        const audioPlayer = audioPlayerRef.current;
        audioPlayer.stretch = baseBpm / bpm;
        setState((state) => (state ? { ...state, bpm } : state));
      }
    },
    [baseBpm]
  );

  const back2TheStart = React.useCallback(() => {
    if (audioPlayerRef.current && bpmStartOffset) {
      const audioPlayer = audioPlayerRef.current;
      audioPlayer.time = bpmStartOffset;
    }
  }, [bpmStartOffset]);

  return {
    state,
    pause,
    play,
    setStretchFactor,
    setVolume,
    setBPM,
    nudgeForward: nudgeForward,
    nudgeBackward: nudgeBackward,
    seek,
    back2TheStart,
  };
}
