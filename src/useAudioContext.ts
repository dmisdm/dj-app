import React from "react";
// eslint-disable-next-line import/no-webpack-loader-syntax
const moduleUrl = require("worklet-loader!@soundtouchjs/audio-worklet/dist/soundtouch-worklet");

export function useAudioContext() {
  return React.useMemo(() => {
    const ctx = new AudioContext();
    ctx.audioWorklet.addModule(moduleUrl);

    return ctx;
  }, []);
}
