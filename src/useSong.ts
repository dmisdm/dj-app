import { useDownloadAudio } from "./useDownloadAudio";

import { useSoundTouchNode } from "./useSoundTouchNode";
import { useAudioContext } from "./useAudioContext";
import { usePhaseVocoderNode } from "./usePhaseVocoderNode";

type Song = {
  play: () => void;
  pause: () => void;
  setTempo: (tempo: number) => void;
  setBPM: (bpm: number) => void;
  setVolume: (volume: number) => void;
  nudgeBackward: () => void;
  nudgeForward: () => void;
  back2TheStart: () => void;
  loading: boolean;
  url: string;
  info?: {
    baseBpm: number;
    bpmStartOffset: number;
    bpm: number;
    tempo: number;
    pitch: number;
    percentagePlayed: number;
    playing: boolean;
    volume: number;
  };
};
export function useSoundTouchSong(url: string, isVideo?: boolean): Song {
  const audioContext = useAudioContext();
  const downloadedAudio = useDownloadAudio(audioContext, url, isVideo);

  const soundTouch = useSoundTouchNode(
    audioContext,
    downloadedAudio.audio?.rawAudioArrayBuffer
  );
  const loading = downloadedAudio.loading || !soundTouch.state;
  return {
    url,
    loading,
    info:
      !downloadedAudio.audio || !soundTouch.state || loading
        ? undefined
        : soundTouch.state,
    setTempo: soundTouch.setTempo,
    setVolume: soundTouch.setVolume,
    play: soundTouch.play,
    pause: soundTouch.pause,
    nudgeForward: soundTouch.nudgeForward,
    nudgeBackward: soundTouch.nudgeBackward,
    setBPM: soundTouch.setBPM,
    back2TheStart: soundTouch.back2TheStart,
  };
}

export function usePhaseVocoderSong(url: string): Song {
  const audioContext = useAudioContext();
  const downloadedAudio = useDownloadAudio(audioContext, url);
  const phaseVocoderNode = usePhaseVocoderNode(
    audioContext,
    audioContext.destination,
    downloadedAudio.audio?.rawAudioArrayBuffer
  );
  const loading = downloadedAudio.loading;
  return {
    url,
    loading,
    info: phaseVocoderNode.state,
    play: phaseVocoderNode.play,
    pause: phaseVocoderNode.pause,
    nudgeBackward: phaseVocoderNode.nudgeBackward,
    nudgeForward: phaseVocoderNode.nudgeForward,
    setTempo: phaseVocoderNode.setStretchFactor,
    setVolume: phaseVocoderNode.setVolume,
    setBPM: phaseVocoderNode.setBPM,
    back2TheStart: phaseVocoderNode.back2TheStart,
  };
}

export const useSong = useSoundTouchSong;
