declare module "@soundtouchjs/audio-worklet" {
  export class SoundTouchNode extends AudioWorkletNode {
    readonly ready: boolean;
    readonly playing: boolean;
    readonly duration: number;
    readonly formattedDuration: string;
    readonly sampleRate: number;
    readonly bufferLength: number;
    readonly numberOfChannels: number;
    readonly audioBuffer?: AudioBuffer;
    pitch?: number;
    pitchSemitones?: number;
    rate?: number;
    tempo?: number;
    percentagePlayed: number;
    // Improve the type of "detail"
    on(
      event: "initialized" | "play" | "end",
      listener: (detail: unknown) => void
    );
    //TODO: off() method
    play();
    pause();
    stop();
    connectToBuffer(): AudioBufferSourceNode;
    disconnectFromBuffer();
  }
  export default function createSoundTouchNode(
    audioCtx: AudioContext,
    nodeClass: typeof AudioWorkletNode,
    rawAudioArrayBuffer: ArrayBuffer,
    options?: AudioWorkletNodeOptions
  ): SoundTouchNode;
}

declare module "soundtouchjs" {
  export class PitchShifter {
    constructor(
      context: AudioContext,
      buffer: AudioBuffer,
      bufferSize: number,
      onEnd?: () => void
    );
    tempo: number;
    connect(node: AudioNode);
    disconnect();
  }
}
