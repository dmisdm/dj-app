import React from "react";
import {
  Box,
  Grid,
  Typography,
  CssBaseline,
  Button,
  Slider,
} from "@material-ui/core";
import { useSong } from "./useSong";

import { Disk } from "./Disk";

function App() {
  //const song1 = useSong("/song4.mp3");
  //const song2 = useSong("/song2.mp3");
  /*   const song2 = useSong(
    "https://r1---sn-u2bpouxgoxu-hxa6.googlevideo.com/videoplayback?expire=1598003418&ei=ekQ_X6zcE82UwgP_yIGYCg&ip=124.168.202.197&id=o-AEKuFUk64lchQPysaBUpHboeg8xSqfAPfi5JVRitzr4v&itag=18&source=youtube&requiressl=yes&mh=wu&mm=31%2C29&mn=sn-u2bpouxgoxu-hxa6%2Csn-ntqe6nel&ms=au%2Crdu&mv=m&mvi=1&pcm2cms=yes&pl=22&gcr=au&initcwndbps=992500&vprv=1&mime=video%2Fmp4&gir=yes&clen=23237328&ratebypass=yes&dur=283.144&lmt=1582853097797215&mt=1597981477&fvip=1&beids=23886205&c=WEB&txp=5531432&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cgcr%2Cvprv%2Cmime%2Cgir%2Cclen%2Cratebypass%2Cdur%2Clmt&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpcm2cms%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRgIhALL6op2Dx_8S95XJHE60eph8rFxAuxY2HJ0dXwGVncj8AiEA4poaVFFLz6ik4VzJ81aPqnmZWJu3LQcHxQKn8WmXEWM%3D&sig=AOq0QJ8wRAIgfh_CkwA3f6u3TH7ZkBB61_OHzXT6KmnIPTXk97gYCfwCIFJKl7OL8x0OfgIyxPXrHtemel38LTeOGkFJtG1fu7Gy",
    true
  ); */
  const song2 = useSong(
    `/api/youtube-dl?link=${encodeURIComponent(
      "https://www.youtube.com/watch?v=i1gVxKhdGPs"
    )}`,
    true
  );
  const song1 = useSong(
    `/api/youtube-dl?link=${encodeURIComponent(
      "https://www.youtube.com/watch?v=sW9y8A4W9DQ"
    )}`,
    true
  );
  const songs = [song1, song2];
  const [globalBpm, setGlobalBpm] = React.useState<number>(100);
  const [globalVolume, setGlobalVolume] = React.useState<number>(1);
  React.useEffect(() => {
    if (song1.info && song2.info) {
      song1.setBPM(globalBpm);
      song2.setBPM(globalBpm);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalBpm]);
  React.useEffect(() => {
    if (song1.info && song2.info) {
      song1.setVolume(globalVolume);
      song2.setVolume(globalVolume);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalVolume]);

  const globalPlayPause = React.useCallback(() => {
    if (song1.info && song2.info) {
      if (!song1.info.playing && !song2.info.playing) {
        song1.play();
        song2.play();
      } else {
        song1.pause();
        song2.pause();
      }
    }
  }, [song1, song2]);

  const globalPlayPauseLabel =
    song1.info && song2.info
      ? !song1.info.playing && !song2.info.playing
        ? "Play"
        : "Pause"
      : undefined;

  const items = songs.map((song) => (
    <Box
      key={song.url}
      display="flex"
      justifyContent="center"
      alignItems="center"
      clone
    >
      <Grid item sm={6}>
        <Box
          position="relative"
          height={300}
          width={300}
          borderRadius="50%"
          display="flex"
          alignItems="center"
          justifyContent="center"
          flexDirection="column"
        >
          <Box clone position="absolute" zIndex={-1}>
            <Disk
              percentagePlayed={song.info?.percentagePlayed}
              loading={song.loading}
            />
          </Box>
          {song.info ? (
            <>
              <Typography variant="caption">BPM</Typography>
              <Slider
                style={{ width: 400 }}
                value={song.info.bpm}
                min={80}
                max={170}
                step={0.1}
                onChange={(e, v) =>
                  typeof v === "number" && song.setTempo(v / 100)
                }
              />
              <Typography variant="caption">Volume</Typography>
              <Slider
                style={{ width: 400 }}
                value={song.info.volume * 100}
                min={0}
                max={200}
                onChange={(e, v) =>
                  typeof v === "number" && song.setVolume(v / 100)
                }
              />
            </>
          ) : null}
          <Typography variant="h4">
            {song.loading || !song.info ? (
              "Loading..."
            ) : (
              <>
                BPM: <small>{song.info.bpm.toFixed(1)}</small>
              </>
            )}
          </Typography>
          <Button
            disabled={song.loading}
            onClick={
              song.loading
                ? () => {}
                : song.info?.playing
                ? song.pause
                : song.play
            }
          >
            {song.loading ? () => {} : song.info?.playing ? "Pause" : "Play"}
          </Button>
          <Box display="flex">
            <Button disabled={song.loading} onClick={song.nudgeBackward}>
              Nudge backward
            </Button>
            <Button disabled={song.loading} onClick={song.nudgeForward}>
              Nudge forward
            </Button>
          </Box>
        </Box>
      </Grid>
    </Box>
  ));
  return (
    <>
      <CssBaseline />

      <Box height="100vh" width="100vw" display="flex" flexDirection="column">
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          flexBasis={300}
        >
          <Box display="flex" flexDirection="column">
            <Typography variant="caption">Global BPM</Typography>

            <Slider
              style={{ width: 700 }}
              value={globalBpm}
              min={80}
              max={170}
              step={0.1}
              onChange={(e, v) => typeof v === "number" && setGlobalBpm(v)}
            />
            <Typography variant="caption">Global Volume</Typography>

            <Slider
              style={{ width: 700 }}
              value={globalVolume}
              min={0}
              max={2}
              step={0.01}
              onChange={(e, v) => typeof v === "number" && setGlobalVolume(v)}
            />
            <Button
              disabled={!globalPlayPauseLabel}
              onClick={() => {
                songs.forEach((song) => {
                  if (song.info) {
                    song.back2TheStart();
                  }
                });
              }}
            >
              {globalPlayPauseLabel ? "Back2thestart" : "Loading..."}
            </Button>
            <Button
              disabled={!globalPlayPauseLabel}
              onClick={() => {
                songs.forEach((song) => {
                  if (song.info) {
                    song.setBPM(song.info.baseBpm);
                  }
                });
              }}
            >
              {globalPlayPauseLabel ? "Reset BPMs" : "Loading..."}
            </Button>
            <Button disabled={!globalPlayPauseLabel} onClick={globalPlayPause}>
              {globalPlayPauseLabel || "Loading..."}
            </Button>
          </Box>
        </Box>
        <Grid direction="row" container>
          {items}
        </Grid>
      </Box>
    </>
  );
}
export default App;
