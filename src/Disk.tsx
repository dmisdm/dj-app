import React from "react";
import { CircularProgress } from "@material-ui/core";
export function Disk(props: {
  percentagePlayed?: number;
  loading?: boolean;
  className?: string;
}) {
  return (
    <CircularProgress
      className={props.className}
      size="100%"
      value={props.percentagePlayed || 0}
      variant={props.loading ? "indeterminate" : "static"}
      style={{ border: "solid 1px #ccc ", borderRadius: "50%" }}
    />
  );
}
