import createSoundTouchNode, {
  SoundTouchNode,
} from "@soundtouchjs/audio-worklet";
import React from "react";
import { guess } from "web-audio-beat-detector";
/**
 * Admittedly this is not the best name. This should be named, maybe, useTimeStretchNode or something (but even then that is an understatement)
 */
export function useSoundTouchNode(
  audioContext: AudioContext,
  rawAudioArrayBuffer?: ArrayBuffer
) {
  const soundtouchRef = React.useRef<SoundTouchNode>();
  const bufferNode = React.useRef<AudioBufferSourceNode>();
  const gainNode = React.useRef<GainNode>();
  const [state, setState] = React.useState<
    | {
        tempo: number;
        rate: number;
        percentagePlayed: number;
        percentagePlayedStartOffset: number;
        pitch: number;
        duration: number;
        playing: boolean;
        bpmStartOffset: number;
        bpm: number;
        baseBpm: number;
        volume: number;
      }
    | undefined
  >();

  const updateStateFromSoundtouch = React.useCallback(() => {
    const soundtouch = soundtouchRef.current;
    if (!soundtouch) return;
    setState((state) =>
      state
        ? {
            ...state,
            bpm: state.baseBpm * state.tempo,
            percentagePlayed: soundtouch.percentagePlayed,
            playing: soundtouch.playing,
          }
        : state
    );
  }, []);

  const volume = state?.volume;

  React.useEffect(() => {
    let cancelled = false;
    if (rawAudioArrayBuffer) {
      const soundtouch = (soundtouchRef.current = createSoundTouchNode(
        audioContext,
        AudioWorkletNode,
        rawAudioArrayBuffer
      ));
      soundtouch.on("initialized", () => {
        if (!cancelled && soundtouch.audioBuffer) {
          guess(soundtouch.audioBuffer).then(({ bpm, offset }) => {
            if (!cancelled) {
              const percentagePlayed = (offset * 60) / soundtouch.duration;

              setState({
                bpmStartOffset: offset,
                baseBpm: bpm,
                bpm: bpm * 1,
                tempo: 1,
                rate: 1,
                percentagePlayed,
                pitch: 1,
                volume: 1,
                playing: soundtouch.playing,
                duration: soundtouch.duration,
                percentagePlayedStartOffset: percentagePlayed,
              });
              soundtouch.percentagePlayed = percentagePlayed;
            }
          });
        }
      });

      soundtouch.on("play", (e) => {
        if (!cancelled) updateStateFromSoundtouch();
      });
      soundtouch.on("end", (e) => {
        if (!cancelled) updateStateFromSoundtouch();
      });
    }

    return () => {
      cancelled = true;
    };
  }, [audioContext, rawAudioArrayBuffer, updateStateFromSoundtouch]);

  const play = React.useCallback(() => {
    if (!soundtouchRef.current?.ready) return;
    if (!bufferNode.current) {
      bufferNode.current = soundtouchRef.current.connectToBuffer();
    }
    if (!gainNode.current) {
      gainNode.current = audioContext.createGain();
      if (volume) gainNode.current.gain.value = volume;
    }

    soundtouchRef.current.connect(gainNode.current);
    gainNode.current.connect(audioContext.destination);
    soundtouchRef.current?.play();
    setState((state) =>
      state
        ? {
            ...state,
            playing: true,
          }
        : state
    );
  }, [audioContext, volume]);
  const pause = React.useCallback(() => {
    if (!state?.playing) return;
    gainNode.current?.disconnect();
    soundtouchRef?.current?.disconnect();
    soundtouchRef.current?.disconnectFromBuffer();
    gainNode.current = undefined;
    bufferNode.current = undefined;
    soundtouchRef.current?.pause();
    setState((state) =>
      state
        ? {
            ...state,

            playing: false,
          }
        : state
    );
  }, [state]);

  const setTempo = React.useCallback((tempo: number) => {
    if (soundtouchRef.current) {
      soundtouchRef.current.tempo = tempo;
      setState((state) =>
        state
          ? {
              ...state,

              tempo,
              bpm: state.baseBpm * tempo,
            }
          : state
      );
    }
  }, []);

  const setVolume = React.useCallback((volume: number) => {
    setState((state) =>
      state
        ? {
            ...state,
            volume,
          }
        : state
    );
  }, []);

  const nudgeForward = React.useCallback(() => {
    if (soundtouchRef.current && state) {
      const soundTouch = soundtouchRef.current;
      const currentTempo = state.tempo;
      const difference = state.tempo - (state.bpm - 10) / state.baseBpm;
      soundTouch.tempo = currentTempo + difference;

      setTimeout(() => {
        soundTouch.tempo = currentTempo;
      }, 50);
    }
  }, [state]);
  const nudgeBackward = React.useCallback(() => {
    if (soundtouchRef.current && state) {
      const soundTouch = soundtouchRef.current;
      const currentTempo = state.tempo;
      const difference = state.tempo - (state.bpm + 10) / state.baseBpm;
      soundTouch.tempo = currentTempo + difference;
      setTimeout(() => {
        soundTouch.tempo = currentTempo;
      }, 50);
    }
  }, [state]);

  React.useEffect(() => {
    if (volume && gainNode.current) {
      gainNode.current.gain.value = volume;
    }
  }, [volume]);

  const setBPM = React.useCallback(
    (bpm: number) => {
      if (soundtouchRef.current && state) {
        const soundtouch = soundtouchRef.current;

        const baseBpm = state.baseBpm;
        const newTempo = (soundtouch.tempo = bpm / baseBpm);
        setState((state) =>
          state ? { ...state, bpm, tempo: newTempo } : state
        );
      }
    },
    [state]
  );

  const back2TheStart = React.useCallback(() => {
    if (soundtouchRef.current && state) {
      const soundtouch = soundtouchRef.current;

      soundtouch.percentagePlayed = state.percentagePlayedStartOffset;
      setState((state) =>
        state
          ? { ...state, percentagePlayed: state.percentagePlayedStartOffset }
          : state
      );
    }
  }, [state]);

  return React.useMemo(
    () => ({
      state,
      play,
      pause,
      setTempo,
      setVolume,
      nudgeForward,
      nudgeBackward,
      setBPM,
      back2TheStart,
    }),
    [
      state,
      play,
      pause,
      setTempo,
      setVolume,
      nudgeForward,
      nudgeBackward,
      setBPM,
      back2TheStart,
    ]
  );
}
