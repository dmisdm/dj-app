import { ObservableState } from "./ObservableState";

export type State = {
  tempo: number;
  secondsPlayed: number;
  durationSeconds: number;
  percentagePlayed: number;
  pitch: number;
  playing: boolean;
  bpmStartOffset: number;
  bpm: number;
  baseBpm: number;
  volume: number;
};
export abstract class AudioSourceProcessor extends ObservableState<State> {
  constructor(
    public context: AudioContext,
    public buffer: AudioBuffer,
    public destination: AudioNode,
    initialState: State
  ) {
    //TODO
    super(initialState);
  }

  abstract readonly node: AudioNode;

  abstract setTempo: (tempo: number) => void;

  abstract setVolume: (volume: number) => void;

  abstract setBpm: (bpm: number) => void;

  play = () => {
    this.node.connect(this.destination);
  };

  stop = () => {
    this.node.disconnect();
  };
}
