import { InitParams, UpdateEventPayload } from "./worklet";

export class AudioPlayer {
  private _audioContext: AudioContext;
  private samplePosition = 0;
  private synSamplePosition = 0;
  private _node: AudioWorkletNode;
  private stretchFactor = 1;
  private playing = false;
  private _loaded = false;
  private _buffer: AudioBuffer;
  private _destination: AudioNode;

  constructor(params: {
    audioContext: AudioContext;
    audioBuffer: AudioBuffer;
    destination: AudioNode;
    bufferSize?: number;
    sampleRate?: number;
    div?: number;
  }) {
    this._destination = params.destination;
    this._audioContext = params.audioContext
      ? params.audioContext
      : new AudioContext();
    const worklet = new AudioWorkletNode(params.audioContext, "phase-vocoder", {
      outputChannelCount: [2],
    });
    this._node = worklet;
    this._buffer = params.audioBuffer;
    this._loaded = true;
    this._node.port.postMessage({
      type: "INIT",
      payload: {
        leftBuffer: this._buffer.getChannelData(0),
        rightBuffer: this._buffer.getChannelData(1),
        bufferLength: this._buffer.length,
        sampleRate: this._buffer.sampleRate,
      } as InitParams,
    });
    this._node.port.onmessage = this.messageHandler;
  }
  messageHandler = (e: MessageEvent) => {
    const { type, payload } = e.data;
    switch (type) {
      case "UPDATE":
        this.onWorkerUpdated(payload);
        break;
    }
  };
  onWorkerUpdated = (update: UpdateEventPayload) => {
    Object.assign(this, update);
  };
  postMessage = (data: any) => this._node.port.postMessage(data);
  get time() {
    return this.synSamplePosition / this._buffer.sampleRate;
  }
  set time(position) {
    this.postMessage({ type: "SEEK", payload: position });
  }
  get duration() {
    return this._buffer.duration;
  }
  get stretch() {
    return this.stretchFactor;
  }
  set stretch(factor) {
    this.postMessage({ type: "STRETCH", payload: factor });
  }

  get audio() {
    return this._buffer;
  }
  get isPlaying() {
    return this.playing;
  }
  get isLoaded() {
    return this._loaded;
  }

  play = () => {
    if (this._audioContext.state === "suspended") {
      this._audioContext.resume();
    }
    this.playing = true;
    this._node.port.postMessage({
      type: "PLAY",
    });

    this._node.connect(this._destination);
  };
  stop = () => {
    this.playing = false;
    this._node.port.postMessage({
      type: "STOP",
    });
    this._node.disconnect();
  };
}
