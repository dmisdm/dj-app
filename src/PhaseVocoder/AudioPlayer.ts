import { PhaseVocoder } from "./PV";

export class AudioPlayer {
  private _audioContext: AudioContext;
  private _samplePosition = 0;
  private _synSamplePosition = 0;
  private _bufferSize: number;
  private _sampleRate: number;
  private _pvL: PhaseVocoder;
  private _pvR: PhaseVocoder;
  private _outBufferL: Array<number> = [];
  private _outBufferR: Array<number> = [];
  private _node: ScriptProcessorNode;
  private _stretchFactor = 1;
  private _pitchFactor = 1;
  private _playing = false;
  private _loaded = false;
  private _buffer: AudioBuffer;
  private _destination: AudioNode;

  constructor(params: {
    audioContext: AudioContext;
    audioBuffer: AudioBuffer;
    destination: AudioNode;
    bufferSize?: number;
    sampleRate?: number;
    div?: number;
  }) {
    this._destination = params.destination;
    this._audioContext = params.audioContext
      ? params.audioContext
      : new AudioContext();
    this._bufferSize = params.bufferSize || 4096;
    this._sampleRate = params.sampleRate || 44100;
    var _div = params.div || 2;
    this._pvL = new PhaseVocoder(this._bufferSize / _div, this._sampleRate);
    this._pvL.init();
    this._pvR = new PhaseVocoder(this._bufferSize / _div, this._sampleRate);
    this._pvR.init();
    this._node = this._audioContext.createScriptProcessor(
      this._bufferSize,
      2,
      2
    );
    this._buffer = params.audioBuffer;
    this._loaded = true;

    this._node.onaudioprocess = (e) => {
      if (!this._playing || !this._loaded) return;

      var il = this._buffer!.getChannelData(0);
      var ir = this._buffer!.getChannelData(1);

      var ol = e.outputBuffer.getChannelData(0);
      var or = e.outputBuffer.getChannelData(1);

      // Fill output buffers (left & right) until the system has
      // enough processed samples to reproduce.
      do {
        if (!this._playing || !this._loaded) return;

        if (
          this._samplePosition / this._sampleRate >= this._buffer!.duration ||
          !this._loaded
        )
          break;

        var bufL = il.subarray(
          this._samplePosition,
          this._samplePosition + this._bufferSize / _div
        );
        var bufR = ir.subarray(
          this._samplePosition,
          this._samplePosition + this._bufferSize / _div
        );

        this._samplePosition += this._pvL.get_analysis_hop();
        this._synSamplePosition += this._pvL.get_synthesis_hop();

        // Process left input channel
        this._outBufferL = this._outBufferL.concat(this._pvL.process(bufL));

        // Process right input channel
        this._outBufferR = this._outBufferR.concat(this._pvR.process(bufR));
      } while (this._outBufferL.length < this._bufferSize);

      ol.set(this._outBufferL.splice(0, this._bufferSize));
      or.set(this._outBufferR.splice(0, this._bufferSize));
    };
  }

  get time() {
    return this._synSamplePosition / this._sampleRate;
  }
  set time(position) {
    var __samplePosition = position * this._sampleRate;
    if (!this._loaded)
      throw Error(
        "Load an audio buffer in AudioPlayer before changing the current time"
      );
    if (__samplePosition >= this._buffer!.duration)
      this._samplePosition = this._buffer!.length;
    else this._samplePosition = __samplePosition;
  }

  get duration() {
    return this._buffer.duration;
  }
  get stretch() {
    return this._stretchFactor;
  }
  set stretch(factor) {
    this._stretchFactor = factor;
    this._pvL.set_alpha(this._stretchFactor);
    this._pvR.set_alpha(this._stretchFactor);
  }
  get pitch() {
    return this._pitchFactor;
  }
  set pitch(factor) {
    this._pitchFactor = factor;
  }
  get audio() {
    return this._buffer;
  }
  get isPlaying() {
    return this._playing;
  }
  get isLoaded() {
    return this._loaded;
  }

  play = () => {
    if (this._audioContext.state === "suspended") {
      this._audioContext.resume();
    }

    this._playing = true;
    this._node.connect(this._destination);
  };
  stop = () => {
    this._playing = false;
    this._node.disconnect();
  };
}
