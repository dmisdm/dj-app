import { PhaseVocoder } from "./PV";
export type InitParams = {
  sampleRate: number;
  duration: number;
  bufferLength: number;
  leftBuffer: Float32Array;
  rightBuffer: Float32Array;
};

export type UpdateEventPayload = {
  samplePosition: number;
  synSamplePosition: number;
  playing: boolean;
  stretchFactor: number;
};
const BUFFER_SIZE = 128;
const PV_WIN_SIZE = 2048;
// @ts-ignore
interface State {
  leftBuffer: Float32Array;
  rightBuffer: Float32Array;
  phaseVocoderLeft: PhaseVocoder;
  phaseVocoderRight: PhaseVocoder;
  sampleRate: number;
  duration: number;
  bufferLength: number;
}

class PhaseVocoderWorklet extends AudioWorkletProcessor {
  private _playing = false;
  private _loaded = false;
  private state?: State;
  private _samplePosition = 0;
  private _synSamplePosition = 0;
  private _stretchFactor = 1;
  private _position = 0;
  private _outBufferL: Array<number> = [];
  private _outBufferR: Array<number> = [];

  constructor() {
    super();
    this.port.onmessage = this.messageProcessor;
  }

  messageProcessor = (event: MessageEvent) => {
    const { type, payload } = event.data;
    switch (type) {
      case "INIT":
        this.init(payload);
        break;
      case "PLAY":
        this.play();
        break;
      case "SEEK":
        this.seek(payload);
        break;
      case "STRETCH":
        this.stretch(payload);
        break;
    }
  };

  stretch = (factor: number) => {
    if (!this.state || !this._loaded) return;
    this._stretchFactor = factor;
    this.state.phaseVocoderLeft.set_alpha(this._stretchFactor);
    this.state.phaseVocoderRight.set_alpha(this._stretchFactor);
    this.notifyUpdate();
  };

  play = () => {
    this._playing = true;
    this.notifyUpdate();
  };

  stop = () => {
    this._playing = true;
    this.notifyUpdate();
  };
  seek = (position: number) => {
    if (!this._loaded || !this.state)
      throw Error(
        "Load an audio buffer in AudioPlayer before changing the current time"
      );
    var __samplePosition = position * this.state.sampleRate;
    if (__samplePosition >= this.state.duration)
      this._samplePosition = this.state.bufferLength;
    else {
      this._samplePosition = __samplePosition;
      this._position = __samplePosition;
    }
    this.notifyUpdate();
  };

  init = (params: InitParams) => {
    const phaseVocoderLeft = new PhaseVocoder(PV_WIN_SIZE, params.sampleRate);
    phaseVocoderLeft.init();
    const phaseVocoderRight = new PhaseVocoder(PV_WIN_SIZE, params.sampleRate);
    phaseVocoderRight.init();
    this.state = {
      duration: params.duration,
      leftBuffer: params.leftBuffer,
      rightBuffer: params.rightBuffer,
      phaseVocoderLeft,
      phaseVocoderRight,
      sampleRate: params.sampleRate,
      bufferLength: params.bufferLength,
    };
    this._loaded = true;
  };

  notifyUpdate = () => {
    this.port.postMessage({
      type: "UPDATE",
      payload: {
        samplePosition: this._samplePosition,
        synSamplePosition: this._synSamplePosition,
        playing: this._playing,
        stretchFactor: this._stretchFactor,
      } as UpdateEventPayload,
    });
  };

  process(inputs: Float32Array[][], outputs: Float32Array[][]) {
    if (!this._playing || !this._loaded || !this.state) return true;

    var il = this.state.leftBuffer;
    var ir = this.state.rightBuffer;

    var ol = outputs[0][0];
    var or = outputs[0][1];
    /* 
// Example of just straight audio
    ol.set(il.subarray(this._position, this._position + BUFFER_SIZE))
    or.set(ir.subarray(this._position, this._position + BUFFER_SIZE))
    this._position += BUFFER_SIZE */

    // Fill output buffers (left & right) until the system has
    // enough processed samples to reproduce.
    do {
      if (!this._playing || !this._loaded) return true;

      if (
        this._samplePosition / this.state.sampleRate >= this.state.duration ||
        !this._loaded
      )
        break;

      var bufL = il.subarray(
        this._samplePosition,
        this._samplePosition + PV_WIN_SIZE
      );
      var bufR = ir.subarray(
        this._samplePosition,
        this._samplePosition + PV_WIN_SIZE
      );

      this._samplePosition += this.state.phaseVocoderRight.get_analysis_hop();
      this._synSamplePosition += this.state.phaseVocoderLeft.get_synthesis_hop();

      // Process left input channel
      this._outBufferL = this._outBufferL.concat(
        this.state.phaseVocoderLeft.process(bufL)
      );

      // Process right input channel
      this._outBufferR = this._outBufferR.concat(
        this.state.phaseVocoderRight.process(bufR)
      );
    } while (this._outBufferL.length < BUFFER_SIZE);

    ol.set(this._outBufferL.splice(0, BUFFER_SIZE));
    or.set(this._outBufferR.splice(0, BUFFER_SIZE));
    this.notifyUpdate();
    return true;
  }
}
registerProcessor("phase-vocoder", PhaseVocoderWorklet);
