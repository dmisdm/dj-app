import React from "react";
import { guess } from "web-audio-beat-detector";
export function useBPM(audioBuffer?: AudioBuffer) {
  const [state, setState] = React.useState<{ loading: boolean; bpm?: number }>({
    loading: true,
  });
  React.useEffect(() => {
    let cancelled = false;
    if (audioBuffer) {
      guess(audioBuffer).then(
        ({ bpm }) => !cancelled && setState({ loading: false, bpm })
      );
    }

    return () => {
      cancelled = true;
    };
  }, [audioBuffer]);

  return state;
}
