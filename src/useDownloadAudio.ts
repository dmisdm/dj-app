import React from "react";

export function useDownloadAudio(
  ctx: AudioContext,
  url: string,
  isVideo?: boolean
) {
  const [state, setState] = React.useState<{
    loading: boolean;
    audio?: {
      rawAudioArrayBuffer: ArrayBuffer;
    };
  }>({ loading: true });
  React.useEffect(() => {
    let cancelled = false;
    fetch(url).then(async (r) => {
      const arrayBuffer = await r.arrayBuffer();

      if (!cancelled) {
        setState({
          loading: false,
          audio: { rawAudioArrayBuffer: arrayBuffer },
        });
      }
    });
    return () => {
      cancelled = true;
    };
  }, [ctx, isVideo, url]);
  return state;
}
